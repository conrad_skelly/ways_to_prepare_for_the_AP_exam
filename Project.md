# Preparing for the AP Exam

## Plan to prepare

1. Make sure you fully understand the task at hand.

2. Study the 5 Big Ideas.

    **Big Idea 1:** Creative Development
    
    **Big Idea 2:** Data
    
    **Big Idea 3:** Algorithms and Programming
    
    **Big Idea 4:** Computer Systems and Networks
   
    **Big Idea 5:** Impact of Computing

3. Extra practice and studying.

    - Study other materials like your notes and other sources.
    - Complete some practice tests.

### What is the AP Computer Science Exam?

The AP computer science exam is the final exam of the course AP computer science. This exam is a 70 question multiple choice test. Like all other AP courses, this test has five possible grades: 5, 4, 3, 2, and 1. To pass the exam, all you need is to get a 3. This test needs to be passed to pass the course, because of this, this test is very important.

### Format of the Test

As previously said in the last section, the exam itself is a 70 question multiple choice test. The other part of the exam is known as the Create Performance Task (CPT). This document does not focus on the CPT, it only focuses on the exam itself. 70% of the points come from the mulitlbe choice test and 30% of the points come from the responces to the programing questions.

# A deep dive into the collage borad website

![Screen Shot 2023-03-14 at 11](images/image.png)
In the collage board website you can see code samples from past years student for the AP test. you can watch a video of the program working [video of program](https://www.youtube.com/watch?v=VTtZi_Cr4Cc) In the video you can see the students written responses to the questions. You can use this if you want an idea about what you are going to code for the AP exam. You can also look into the scoring guidelines, you can get an I dead to what your response shpuld be. Under that is the chief scoring report where you can see how most students peformed on the questions inculding errors, there is also the score distributions if you want to look at that.

### What is the college board stance on plagiarism

If the student used code that someone else created without appropriate acknowledgment through citations attribution or reference is concidered plagiarism any student that commits plagiarism will recive a zero on the performance task.

## Website

[Tutorials](https://www.w3schools.com/) - This site allows you to look at tones of tutorials about stuff you might not know for example functions you can take the tutorial and be all good for when you have to write a function. This site doesn't just have tutorials for python, there is also stuff for java, javascript, c++, etc. If you want to code this in java this is a great place to start

# Conclusion

We will be preparing for the AP exam by looking at what students have done in the past for inpiration for what we sould do looking over what the code should include seeing what mistakes other students have made and checking some tutorials.
 